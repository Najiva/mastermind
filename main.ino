
/* Use File->Load Prog to
   load a different Program
*/

const int LED1=2;
const int LED2=3;
const int LED3=4;
const int LED4=5;
const int BUTTON1=6;
const int BUTTON2=7;
const int BUTTON3=8;
const int BUTTON4=9;
const int BUTTON5=10;

// push counters
int B1, B2, B3, B4;

String outputLine1 = "Welcome to Mastarmind.";
String outputLine2 = "Your goal is to guess my secret combination.";
String temp;
String yourGuess;
String solution;
// calculated difference of the solution and the guess
String difference;

int B1State = 0;
int B2State = 0;
int B3State = 0;
int B4State = 0;
int enterState = 0;

void setup()
{
	B1 = 0;
	B2 = 0;
	B3 = 0;
	B4 = 0;
	pinMode(BUTTON1, INPUT);
	pinMode(BUTTON2, INPUT);
	pinMode(BUTTON3, INPUT);
	pinMode(BUTTON4, INPUT);
	pinMode(BUTTON5, INPUT);
	pinMode(LED1, OUTPUT);
	pinMode(LED2, OUTPUT);
	pinMode(LED3, OUTPUT);
	pinMode(LED4, OUTPUT);
	
	// vymyslim si riesenie -> unconnected analog pin
	randomSeed(analogRead(A0));
	String c1 = String(random(0,9));
	String c2 = String(random(0,9));
	String c3 = String(random(0,9));
	String c4 = String(random(0,9));
	solution = String(c1 + c2 + c3 + c4);
	
}

//boolean debounce(boolean last){
//	boolean current = digitalRead(BUTTON1);
//	if(last!=current){
//		delay(5);
//		current = digitalRead(BUTTON);
//	}
//	return current;
//}

void increaseButtonCounter(const int bID){
	switch(bID){
		case BUTTON1:
			B1++;
			B1 = B1 % 10;
			break;
		case BUTTON2:
			B2++;
			B2 = B2 % 10;	
			break;
		case BUTTON3:
			B3++;
			B3 = B3 % 10;	
			break;
		case BUTTON4:
			B4++;
			B4 = B4 % 10;	
			break;	
	}
	String s1 = String(B1, DEC);
	String s2 = String(B2, DEC);
	String s3 = String(B3, DEC);
	String s4 = String(B4, DEC);
	yourGuess = String(s1 + s2 + s3 + s4);
}

void compareStrings(){
	int A = 0;
	int B = 0;
	
	// how many colors do i have right?
	for(int i=0; i < 4; i++){
		for(int j=0; j<4;j++){
			if(yourGuess.charAt(i)==solution.charAt(j)) B++;
			}
		} 
		// how many correct colours as well as placements do i have?
		for(int i=0; i < 4; i++){
			
			if(yourGuess.charAt(i)==solution.charAt(i)) A++;
			}
			
			String sA = String(A);
			String sB = String(B-A);
			difference = String(sA + sB);
			
		}
		
		void calculateNewResult(){
			temp = "Zeroing counters...";
			B1 = 0;
			B2 = 0;
			B3 = 0;
			B4 = 0;
			
			compareStrings();
			
			yourGuess = "0000";
		}
		
		void loop()
		{
			
			B1State = digitalRead(BUTTON1);
			B2State = digitalRead(BUTTON2);
			B3State = digitalRead(BUTTON3);
			B4State = digitalRead(BUTTON4);
			enterState = digitalRead(BUTTON5);
			
			if(B1State==LOW) increaseButtonCounter(BUTTON1);
				if(B2State==LOW) increaseButtonCounter(BUTTON2);
					if(B3State==LOW) increaseButtonCounter(BUTTON3);
						if(B4State==LOW) increaseButtonCounter(BUTTON4);
							
			if(enterState==LOW) calculateNewResult();
				delay(100);
			//currentButton = debounce(lastButton);
			//if(lastButton == LOW && currentButton == HIGH){
			//	ledOn = !ledOn;
			//}
			//lastButton = currentButton;
			//digitalWrite(LED1, ledOn);
			
		}
